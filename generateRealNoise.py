#!/usr/bin/env python
# coding: utf-8

import os
import sys
import glob
import h5py
import argparse
import logging

import numpy as np
import scipy.signal as sig

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
import gwpy
from gwpy.timeseries import TimeSeries

from bbhnet_sim import utils

logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.INFO, stream=sys.stdout)

def getStrain(ifo, frame_start, frame_stop, sample_rate, high_pass=None):
    ''' Get strain from [frame_start, frame_stop) from GWOSC, resample,
    and apply high-pass filter '''

    strain = TimeSeries.fetch_open_data(ifo, frame_start, frame_stop)
    strain = strain.resample(sample_rate)
    if high_pass is not None:
        strain = strain.highpass(high_pass)
    return strain

# Parse command-line arguments
def parse_cmd():
    parser = argparse.ArgumentParser()

    # sim args:
    parser.add_argument('-S', '--signal', action='store_true',
                        help='Enable to add GW signal on top of background noise')
    parser.add_argument('-fmin', '--high-pass', type=float, required=False,
                        help='frequency of highpass filter')
    parser.add_argument('-p', '--prior-file', required=False,
                        help='path to prior config file. Required for signal simulation')
    parser.add_argument('--correlation-shift', type=int, required=False,
                        help='if given, also compute the correlation with given shift value')
    parser.add_argument('-s', '--seed', type=int, required=False,
                        help='random seed for reproducibility')


    parser.add_argument('-ns', '--n-samples', required=True,
                        help='Number of samples to inject')

    return parser.parse_args()


if __name__ == '__main__':
    ''' Start simulation '''

    # parse command-line arguments
    FLAGS = parse_cmd()

    outdir = '/home/william.benoit/BBHnetInjections/InjectedFrames/LowSNR/'

    start_times = np.loadtxt('/home/william.benoit/BBHnetInjections/GoodFrameTimes.txt', dtype=int)

    N_samples = int(FLAGS.n_samples)

    #Allows for testing by injecting only a few files. Set it to a large number to inject everything
    files_to_inject = 1000
    count_injected = 0
    for frame_start in start_times:

        frame_start = int(frame_start)
        frame_duration = 4096
        frame_stop = frame_start + frame_duration

        hfile = 'H-H1_GWOSC_O2_4KHZ_R1-{}-{}.gwf'.format(frame_start, frame_duration)
        lfile = 'L-L1_GWOSC_O2_4KHZ_R1-{}-{}.gwf'.format(frame_start, frame_duration)

        # get strain and PSD strain from GWOSC. For some reason, need the +epsilon to make it work
        H1_strain = TimeSeries.fetch_open_data('H1', frame_start, frame_stop + 1e-5)
        L1_strain = TimeSeries.fetch_open_data('L1', frame_start, frame_stop + 1e-5)

        #Changing channel name to match what comes from downloading GWOSC data directly
        # channel = 'GWOSC-4KHZ_R1_STRAIN'
        # H1_strain.channel = gwpy.detector.Channel('H1:'+ channel)
        # H1_strain.channel = 'H1:' + channel
        # L1_strain.channel = 'L1:' + channel

        sample_rate = int(H1_strain.sample_rate.value)
        fftlength = int(max(2, np.ceil(2048 / sample_rate)))

        logging.info('Read strain from {} .. {}'.format(frame_start, frame_stop))

        # calculate the PSD
        H1_psd = H1_strain.psd(fftlength)
        L1_psd = L1_strain.psd(fftlength)
        # H1_strain = H1_strain.whiten(asd=np.sqrt(H1_psd))
        # L1_strain = L1_strain.whiten(asd=np.sqrt(L1_psd))

        # H1_strain = H1_strain.crop(frame_start + 4, frame_stop - 4)
        # L1_strain = L1_strain.crop(frame_start + 4, frame_stop - 4)

        gps_start = sorted(np.random.choice(np.arange(16, 4080, 8), size=N_samples,
                                                    replace=False))
        waveform_duration = 8

        # log and print out some simulation parameters
        logging.info('Simulation parameters')
        logging.info('Adding CBC signals? {}'.format(FLAGS.signal))
        logging.info('Number of samples     : {}'.format(N_samples))
        logging.info('Sample rate [Hz]      : {}'.format(sample_rate))
        logging.info('High pass filter [Hz] : {}'.format(FLAGS.high_pass))

        if FLAGS.signal:
            logging.info('Prior file            : {}'.format(FLAGS.prior_file))

        # set up GW signal parameters
        if FLAGS.signal:
            # define a Bilby waveform generator
            waveform_generator = bilby.gw.WaveformGenerator(
                duration=waveform_duration,
                sampling_frequency=sample_rate,
                frequency_domain_source_model=lal_binary_black_hole,
                parameter_conversion=convert_to_lal_binary_black_hole_parameters,
                waveform_arguments={
                    'waveform_approximant': 'IMRPhenomPv2',
                    'reference_frequency': 50,
                    'minimum_frequency': 20 },
            )

            # sample GW parameters from prior distribution
            priors = bilby.gw.prior.BBHPriorDict(FLAGS.prior_file)
            sample_params = priors.sample(N_samples)

            sample_params['geocent_time'] = gps_start
            frame_size = len(H1_strain)

            # generate GW waveforms
            H1_signals, H1_SNR = utils.generateGW(
                sample_params, 'H1',
                waveform_generator=waveform_generator, get_snr=True, noise_psd=H1_psd)
            L1_signals, L1_SNR = utils.generateGW(
                sample_params, 'L1',
                waveform_generator=waveform_generator, get_snr=True, noise_psd=L1_psd)

            old_SNR = np.sqrt(H1_SNR**2 + L1_SNR**2)
            new_SNR = np.random.uniform(5, 8, len(H1_SNR))

            H1_signals =  H1_signals * (new_SNR / old_SNR)[:, None]
            L1_signals =  L1_signals * (new_SNR / old_SNR)[:, None]
            sample_params['luminosity_distance'] = sample_params['luminosity_distance'] * old_SNR / new_SNR
            H1_SNR = H1_SNR * new_SNR / old_SNR
            L1_SNR = L1_SNR * new_SNR / old_SNR

        if FLAGS.signal:
            # H1_data = TimeSeries(H1_strain, channel = 'test')
            H1_data = H1_strain
            L1_data = L1_strain
            for i in range(N_samples):
                idx1 = gps_start[i]*sample_rate
                idx2 = idx1 + waveform_duration*sample_rate
                H1_data[idx1:idx2] += H1_signals[i]
                L1_data[idx1:idx2] += L1_signals[i]

            H1_data.name = 'H1:GWOSC-4KHZ_R1_STRAIN'
            L1_data.name = 'L1:GWOSC-4KHZ_R1_STRAIN'
            H1_data.write(outdir + 'H1/' + hfile)
            L1_data.write(outdir + 'L1/' + lfile)

        # compute the Pearson coefficient if given
        if FLAGS.correlation_shift is not None:
            correlation = utils.pearson_shift(
                H1_data, L1_data, shift=FLAGS.correlation_shift)

        # Write to output file
        logging.info('Write to file {}'.format(hfile[4:]))
        with h5py.File(outdir + 'h5_files/' + hfile[5:-4] + '.h5', 'w') as f:
            # write data and ASD
            #
            H1_gr = f.create_group('H1')
            # H1_dset = H1_gr.create_dataset('background', data=H1_strain)
            # H1_dset.attrs['channel'] = 'GWOSC'
            # H1_gr.create_dataset('PSD', data=H1_psd.value)
            # H1_gr.create_dataset('freq', data=H1_psd.frequencies.value)

            # # Livingston
            L1_gr = f.create_group('L1')
            # L1_dset = L1_gr.create_dataset('background', data=L1_strain)
            # L1_dset.attrs['channel'] = 'GWOSC'
            # L1_gr.create_dataset('PSD', data=L1_psd.value)
            # L1_gr.create_dataset('freq', data=L1_psd.frequencies.value)

            # if Pearson correlation is computed, also write it
            if FLAGS.correlation_shift is not None:
                gr = f.create_group('Pearson-Correlation')
                gr.create_dataset('correlation', data=correlation)

            f.create_dataset('GPS-start', data=gps_start)

            # write noise attributes
            f.attrs.update({
                'size': N_samples,
                'frame_start': frame_start,
                'frame_stop': frame_stop,
                'sample_rate': sample_rate,
                'psd_fftlength': fftlength,
            })

            # write signals attributes, SNR, and signal parameters
            if FLAGS.signal:
                H1_gr.create_dataset('signal', data=H1_signals)
                L1_gr.create_dataset('signal', data=L1_signals)

                H1_gr.create_dataset('SNR', data=H1_SNR)
                L1_gr.create_dataset('SNR', data=L1_SNR)
                params_gr = f.create_group('signal_params')
                for k, v in sample_params.items():
                    params_gr.create_dataset(k, data=v)

                # Update signal attributes
                f.attrs['waveform_duration'] = 8
                f.attrs['flag'] = 'GW'
            else:
                f.attrs['flag'] = 'BACKGROUND'


        count_injected += 1
        if count_injected == files_to_inject: exit()
