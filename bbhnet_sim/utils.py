import scipy
import numpy as np

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters

from gwpy.timeseries import TimeSeries


### IO function
def dict2args(params, keys=None):
    """ Convert dictionary to commandline argument string """

    # If no key is given, take all keys
    if keys is None:
        keys  = params.keys()

    # Parse
    append = ''
    for key, val in params.items():
        if key not in keys:
            continue
        key = key.replace('_', '-')
        append += f'--{key} '
        if isinstance(val, (list, tuple)):
            for v in val:
                append += str(v)
                append += ' '
        elif val is not None:
            append += str(val)
            append += ' '
    append = append[:-1]  # remove the trailing white space
    return append

### signal processing functions
def getSNR(data, noise_psd, fs, fmin=20):
    ''' Calculate the waveform SNR given the background noise PSD'''
    L = len(data)

    data_fd = np.fft.rfft(data) / fs
    data_freq = np.fft.rfftfreq(L) * fs
    dfreq = data_freq[1] - data_freq[0]

    noise_psd_interp = noise_psd.interpolate(dfreq)
    noise_psd_interp[noise_psd_interp == 0] = 1.

    SNR = 4 * np.abs(data_fd)**2 / noise_psd_interp.value * dfreq
    SNR = np.sum(SNR[fmin <= data_freq])
    SNR = np.sqrt(SNR)

    return SNR

def pearson_shift(x, y, shift):
    ''' Calculate the Pearson correlation coefficient between x and y
    for each array shift value from [0, shift].
    '''
    corr = []
    shift_arr = np.arange(-shift, shift)
    for s in shift_arr:
        x_roll = np.roll(x, s, axis=1)
        mx = x_roll.mean(1, keepdims=True)
        my = y.mean(1, keepdims=True)
        xm, ym = x_roll - mx, y - my
        r_num = np.sum(xm * ym, axis=1)
        r_den = np.sqrt(np.sum(xm**2, axis=1) * np.sum(ym**2, axis=1))
        r = r_num / r_den
        corr.append(r)
    corr = np.stack(corr).T
    return corr

def as_stride(x, input_size, step, shift=0):
    ''' Divide input time series into overlapping chunk '''

    if shift != 0:
        x = np.roll(x, shift)

    noverlap = input_size  - step
    N_sample = (x.shape[-1] - noverlap) // step

    shape = (N_sample, input_size)
    strides = x.strides[:-1]+(step*x.strides[-1], x.strides[-1])
    result = np.lib.stride_tricks.as_strided(x, shape=shape, strides=strides)

    return result

### simulation functions
def generateGW(sample_params, ifo,
               waveform_generator=None, get_snr=False, noise_psd=None,
               whiten_fn=None):
    ''' Generate gravitational-wave events

    Arguments:
    - sample_params: dictionary of GW parameters
    - sample_duration: time duration of each sample
    - triggers: trigger time (relative to `sample_duration`) of each sample
    - ifo: interferometer
    - waveform_generator: bilby.gw.WaveformGenerator with appropriate parameters
    - get_snr: return the SNR of each sample
    - noise_psd: background noise PSD used to calculate SNR or whiten the sample
    - whiten_fn: whiten each sample using the background noise PSD
    '''


    N_sample = len(sample_params['geocent_time'])

    if waveform_generator is None:
        waveform_generator = bilby.gw.WaveformGenerator(
            duration=sample_duration * 2,
            sampling_frequency=16384,
            frequency_domain_source_model=lal_binary_black_hole,
            parameter_conversion=convert_to_lal_binary_black_hole_parameters,
            waveform_arguments={
                'waveform_approximant': 'IMRPhenomPv2',
                'reference_frequency': 50,
                'minimum_frequency': 20 }
            ,
        )

    # if isinstance(triggers, (int, float)):
    #     triggers = np.repeat(triggers, N_sample)

    sample_rate = waveform_generator.sampling_frequency
    waveform_duration = waveform_generator.duration
    waveform_size = int(sample_rate * waveform_duration)

    signals = np.zeros((N_sample, waveform_size))
    SNR = np.zeros(N_sample)

    ifo = bilby.gw.detector.get_empty_interferometer(ifo)
    for i in range(N_sample):
        # Get parameter for one signal
        p = dict()
        for k, v in sample_params.items():
            p[k] = v[i]
        ra, dec, geocent_time, psi = p['ra'], p['dec'], p['geocent_time'], p['psi']

        # Generate signal in IFO
        polarizations = waveform_generator.time_domain_strain(p)
        signal = np.zeros(waveform_size)
        b, a = scipy.signal.butter(N=8, Wn=waveform_generator.waveform_arguments['minimum_frequency'], btype='highpass',
                                   fs=waveform_generator.sampling_frequency)
        for mode in polarizations.keys():
            # Get H1 response
            response = ifo.antenna_response(ra, dec, geocent_time, psi, mode)
            polarizations[mode] = scipy.signal.filtfilt(b, a, polarizations[mode])
            signal += polarizations[mode] * response

        # Total shift = shift to trigger time + geometric shift
        dt = waveform_duration / 2.
        dt += ifo.time_delay_from_geocenter(ra, dec, geocent_time)
        signal = np.roll(signal, int(np.round(dt*sample_rate)))

        # Calculate SNR and whiten signal
        if noise_psd is not None:
            if get_snr:
                SNR[i] = getSNR(signal, noise_psd, sample_rate)
            # if whiten_fn is None:
            #     signal = TimeSeries(signal, dt=1./sample_rate).whiten(
            #         asd=np.sqrt(noise_psd)).value
            # else:
            #     signal = whiten_fn(signal, noise_psd, sample_rate)

        # Truncate signal
        # idx_start = (waveform_size - sample_size) // 2
        # idx_stop = idx_start + sample_size
        signals[i] = signal
    if get_snr:
        return signals, SNR
    return signals

