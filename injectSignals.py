#!/usr/bin/env python
# coding: utf-8

import os
import sys
import glob
import h5py
import argparse
import logging

import numpy as np
import scipy.signal as sig

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
import gwpy
from gwpy.timeseries import TimeSeries

from bbhnet_sim import utils

logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.INFO, stream=sys.stdout)

def getStrain(ifo, frame_start, frame_stop, sample_rate, high_pass=None):
    ''' Get strain from [frame_start, frame_stop) from GWOSC, resample,
    and apply high-pass filter '''

    strain = TimeSeries.fetch_open_data(ifo, frame_start, frame_stop)
    strain = strain.resample(sample_rate)
    if high_pass is not None:
        strain = strain.highpass(high_pass)
    return strain

# Parse command-line arguments
def parse_cmd():
    parser = argparse.ArgumentParser()

    # sim args:
    parser.add_argument('-fmin', '--high-pass', type=float, required=False,
                        help='frequency of highpass filter', default=None)
    parser.add_argument('-fs', '--sample-rate', type=float, required=False,
                        help='Sample rate of the strain', default=4096)
    parser.add_argument('-fref', '--reference-freq', type=float, required=False,
                        help='Reference frequency for waveform generation', default=50)
    parser.add_argument('-p', '--prior-file', required=True,
                        help='path to prior config file')
    parser.add_argument('-ns', '--n-samples', required=True,
                        help='Number of samples to inject')
    parser.add_argument('-d', '--waveform_dur', required=False,
                        help='Waveform duration in seconds', default=8)
    parser.add_argument('-t0', '--frame-start', required=True,
                        help='Frame start time')
    parser.add_argument('-o', '--outdir', required=True,
                        help='Output directory for injected frame files')

    return parser.parse_args()


if __name__ == '__main__':
    ''' Start simulation '''

    # parse command-line arguments
    FLAGS = parse_cmd()

    outdir = FLAGS.outdir

    frame_start = int(FLAGS.frame_start)
    sample_rate = int(FLAGS.sample_rate)
    fftlength = int(max(2, np.ceil(2048 / sample_rate)))

    N_samples = int(FLAGS.n_samples)
    frame_duration = 4096
    frame_stop = frame_start + frame_duration

    hfile = 'H-H1_GWOSC_O2_4KHZ_R1-{}-{}.gwf'.format(frame_start, frame_duration)
    lfile = 'L-L1_GWOSC_O2_4KHZ_R1-{}-{}.gwf'.format(frame_start, frame_duration)

    H1_strain = getStrain('H1', frame_start, frame_stop, sample_rate, FLAGS.high_pass)
    L1_strain = getStrain('L1', frame_start, frame_stop, sample_rate, FLAGS.high_pass)

    logging.info('Read strain from {} .. {}'.format(frame_start, frame_stop))

    H1_psd = H1_strain.psd(fftlength)
    L1_psd = L1_strain.psd(fftlength)

    waveform_duration = int(FLAGS.waveform_duration)
    gps_start = sorted(np.random.choice(np.arange(16, 4080, waveform_duration), size=N_samples,
                                                    replace=False))

    # log and print out some simulation parameters
    logging.info('Simulation parameters')
    logging.info('Number of samples     : {}'.format(N_samples))
    logging.info('Sample rate [Hz]      : {}'.format(sample_rate))
    logging.info('High pass filter [Hz] : {}'.format(FLAGS.high_pass))

    logging.info('Prior file            : {}'.format(FLAGS.prior_file))

    # set up GW signal parameters
    # define a Bilby waveform generator
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=waveform_duration,
        sampling_frequency=sample_rate,
        frequency_domain_source_model=lal_binary_black_hole,
        parameter_conversion=convert_to_lal_binary_black_hole_parameters,
        waveform_arguments={
            'waveform_approximant': 'IMRPhenomPv2',
            'reference_frequency': FLAGS.reference_freq,
            'minimum_frequency': 20 },
        )

    # sample GW parameters from prior distribution
    priors = bilby.gw.prior.BBHPriorDict(FLAGS.prior_file)
    sample_params = priors.sample(N_samples)

    sample_params['geocent_time'] = gps_start

# Put this in a loop?
    # generate GW waveforms
    H1_signals, H1_SNR = utils.generateGW(
        sample_params, 'H1',
        waveform_generator=waveform_generator, get_snr=True, noise_psd=H1_psd)
    L1_signals, L1_SNR = utils.generateGW(
        sample_params, 'L1',
        waveform_generator=waveform_generator, get_snr=True, noise_psd=L1_psd)

    old_SNR = np.sqrt(H1_SNR**2 + L1_SNR**2)
    new_SNR = np.random.uniform(5, 8, len(H1_SNR))

    H1_signals =  H1_signals * (new_SNR / old_SNR)[:, None]
    L1_signals =  L1_signals * (new_SNR / old_SNR)[:, None]
    sample_params['luminosity_distance'] = sample_params['luminosity_distance'] * old_SNR / new_SNR
    H1_SNR = H1_SNR * new_SNR / old_SNR
    L1_SNR = L1_SNR * new_SNR / old_SNR

    H1_data = H1_strain
    L1_data = L1_strain
    for i in range(N_samples):
        idx1 = gps_start[i]*sample_rate
        idx2 = idx1 + waveform_duration*sample_rate
        H1_data[idx1:idx2] += H1_signals[i]
        L1_data[idx1:idx2] += L1_signals[i]

    H1_data.name = 'H1:GWOSC-4KHZ_R1_STRAIN'
    L1_data.name = 'L1:GWOSC-4KHZ_R1_STRAIN'
    H1_data.write(outdir + 'H1/' + hfile)
    L1_data.write(outdir + 'L1/' + lfile)

    # Write to output file
    logging.info('Write to file {}'.format(hfile[4:]))
    with h5py.File(outdir + 'h5_files/' + hfile[5:-4] + '.h5', 'w') as f:
        H1_gr = f.create_group('H1')
        L1_gr = f.create_group('L1')

        f.create_dataset('GPS-start', data=gps_start)

        # write noise attributes
        f.attrs.update({
            'size': N_samples,
            'frame_start': frame_start,
            'frame_stop': frame_stop,
            'sample_rate': sample_rate,
            'psd_fftlength': fftlength,
        })

        # write signals attributes, SNR, and signal parameters
        H1_gr.create_dataset('signal', data=H1_signals)
        L1_gr.create_dataset('signal', data=L1_signals)

        H1_gr.create_dataset('SNR', data=H1_SNR)
        L1_gr.create_dataset('SNR', data=L1_SNR)
        params_gr = f.create_group('signal_params')
        for k, v in sample_params.items():
            params_gr.create_dataset(k, data=v)

        # Update signal attributes
        f.attrs['waveform_duration'] = 8
        f.attrs['flag'] = 'GW'
